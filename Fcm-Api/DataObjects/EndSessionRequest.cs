﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fcm_Api.DataObjects
{
    public class EndSessionRequest
    {
        public String RegistrationToken { get; set; }
    }
}