﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fcm_Api.DataObjects
{
    public class CreateSessionRequest
    {
        public String NotifyTopic { get; set; }
        public int TicketNumber { get; set; }
        public String RegistrationToken { get; set; }
    }
}