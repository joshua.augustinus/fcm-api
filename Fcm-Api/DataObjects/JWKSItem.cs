﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fcm_Api.DataObjects
{
    public class JWKSItem
    {
        /// <summary>
        /// Exponent
        /// </summary>
        public String e { get; set; }

        /// <summary>
        /// Modulus
        /// </summary>
        public String n { get; set; }
        public String kty { get; set; }
        public String use { get; set; } //Optional parameter either sig or enc.
        public JWKSItem(String exponent, String modulus)
        {
            kty = "RSA";
            e = exponent;
            n = modulus;
            use = "sig"; //sig means that the key is NOT used for encryption
        }

        //Empty constructor required for serialization
        public JWKSItem()
        {

        }
    }
}