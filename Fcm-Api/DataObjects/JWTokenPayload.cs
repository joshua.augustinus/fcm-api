﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fcm_Api.DataObjects
{
    public class JWTokenPayload
    {
        public String sub { get; set; }
        public String exp { get; set; }
    }
}