﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fcm_Api.DataObjects
{
    public class OpenTokInfo
    {
        public int ApiKey { get; set; }
        public String SessionId { get; set; }
        public String Token { get; set; }
        public int TicketNumber { get; set; }

        public Dictionary<String, String> ToDictionary()
        {
            var result = new Dictionary<String, String>();
            result.Add("apiKey", ApiKey.ToString());
            result.Add("sessionId", SessionId);
            result.Add("token", Token);
            result.Add("ticketNumber", TicketNumber.ToString());
            return result;
        }


    }
}