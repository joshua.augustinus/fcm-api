﻿using Fcm_Api.DataObjects;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using Tms.Api.Common;

namespace Fcm_Api.Helpers
{
    public class Decrypter
    {
        public JWKSItem PublicKey { get; private set; }
        private static Decrypter _instance = new Decrypter();
        public static Decrypter GetInstance()
        {
            return _instance;
        }

        private Decrypter()
        {

        }

        public async Task Setup(String authUrl)
        {
            using (var client = new ApiClient(authUrl))
            {
            
                client.AcceptJson();
                //will throw if not successstatuscode
                var result = await client.GetObjectAsync<JsonWebKeySet>("jwks");
                PublicKey = result.keys.First();
            }
        }

        public JWTokenPayload DecodeToken(String token)
        {

            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                var parameters = new RSAParameters();
                parameters.Modulus = Convert.FromBase64String(PublicKey.n);
                parameters.Exponent = Convert.FromBase64String(PublicKey.e);
                rsa.ImportParameters(parameters);
                // This will throw if the signature is invalid
                return Jose.JWT.Decode<JWTokenPayload>(token, rsa, Jose.JwsAlgorithm.RS256);
            }
        }
    }
}