﻿using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;


namespace Fcm_Api
{
    public class FcmHelper
    {
        public static void Init()
        {
            FirebaseApp.Create(new AppOptions() {
                Credential = GetCredential()
            });

        }

        private static GoogleCredential GetCredential()
        {
            GoogleCredential credential;

            using (var stream = new FileStream(GetAccountKeyPath(), FileMode.Open, FileAccess.Read))
            {
                credential = GoogleCredential.FromStream(stream).CreateScoped(
            new string[] {
                "https://www.googleapis.com/auth/firebase.messaging" }
            );
            }

            return credential;
        }

        private static FirebaseAdmin.Messaging.Message CreateMessage()
        {
            return new FirebaseAdmin.Messaging.Message()
            {
                Topic = "global",
                Data = new Dictionary<String, String> {
                    {"Test1", "Test2" }
                }
            };
        }


        public static async Task SendMessageAsync()
        {
            try
            {
                var result = await FirebaseMessaging.DefaultInstance.SendAsync(CreateMessage());
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                //Log errror
            }
        }

        public static async Task<object> SendMessageAsync(String topic, Dictionary<String,String> data)
        {
            var message = new FirebaseAdmin.Messaging.Message()
            {
                Topic = topic,
                Data = data,
                Android = new AndroidConfig {Priority = Priority.High}
            };
           

            var result = await FirebaseMessaging.DefaultInstance.SendAsync(message);
            return result;
        }

        public static async Task<object> SendMessageToDeviceAsync(String registrationToken, Dictionary<String,String> data)
        {
            var message = new FirebaseAdmin.Messaging.Message()
            {
                Token = registrationToken,
                Data = data,
                Android = new AndroidConfig { Priority = Priority.High }
            };


            var result = await FirebaseMessaging.DefaultInstance.SendAsync(message);
            return result;
        }

        public static async Task< String> GetAccessToken()
        {
            GoogleCredential credential;
           
            using (var stream = new FileStream(GetAccountKeyPath(), FileMode.Open, FileAccess.Read))
            {
                credential = GoogleCredential.FromStream(stream).CreateScoped(
            new string[] {
                "https://www.googleapis.com/auth/firebase.messaging" }
            );
            }

            

            ITokenAccess c = credential as ITokenAccess;
            var result =  await c.GetAccessTokenForRequestAsync();
            return result;
        }

        private static String GetAccountKeyPath()
        {
            //Get the bin path
            //https://stackoverflow.com/questions/3461865/how-do-i-get-bin-path
            var path = System.IO.Path.GetDirectoryName(
                  System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Substring(6);
            path = Path.Combine(path, "serviceAccountKey.json");

            return path;
        }
    }
}