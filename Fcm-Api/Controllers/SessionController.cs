﻿using Fcm_Api.DataObjects;
using Fcm_Api.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace Fcm_Api.Controllers
{
    [BearerAuth]
    public class SessionController : ApiController
    {
        //Create new session
        [HttpPost]
        [Route("session")]
        public async Task<object> New(CreateSessionRequest sessionRequest)
        {
            if (String.IsNullOrEmpty(BearerAuth.GetPayload().sub))
                return BadRequest("sub on Bearer token cannot be empty");

            if (sessionRequest.TicketNumber <= 0)
                return BadRequest("Ticket Number cannot be empty");

            var helper = new OpenTokHelper();
            var info = helper.CreateSession(sessionRequest.TicketNumber);

            if (sessionRequest != null && !String.IsNullOrEmpty(sessionRequest.RegistrationToken))
            {
                try
                {
                    var dictionary = info.ToDictionary();
                    dictionary.Add("event", "createSession");
                    var fcmResult = await FcmHelper.SendMessageToDeviceAsync(sessionRequest.RegistrationToken, dictionary);
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.BadGateway, e);
                }

            }

            return info;

        }

        [HttpDelete]
        [Route("session")]
        public async Task<object> EndSession(EndSessionRequest request)
        {

            if (String.IsNullOrEmpty(request.RegistrationToken))
                return BadRequest("RegistrationToken cannot be empty");


            var dictionary = new Dictionary<String, String>();
            dictionary.Add("event", "endSession");


            try
            {
                var fcmResult = await FcmHelper.SendMessageToDeviceAsync(request.RegistrationToken, dictionary);
                return Ok(fcmResult);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadGateway, e);
            }



        }
    }
}
