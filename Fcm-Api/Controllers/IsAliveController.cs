﻿using Fcm_Api.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Fcm_Api.Controllers
{
    public class IsAliveController : ApiController
    {
        [HttpPost]
        [HttpGet]
        [Route("isalive")]
        public object IsAlive()
        {
            return "I'm alive";
        }
    }
}
