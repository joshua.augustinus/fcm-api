﻿using Fcm_Api.DataObjects;
using OpenTokSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tms.Api.Common;

namespace Fcm_Api
{
    public class OpenTokHelper
    {
        private OpenTok _openTok;
        private int _apiKey;
        private const String CONFIG = "secrets.config";

        public OpenTokHelper()
        {
            var apiKey = ConfigReader.GetAppSetting(CONFIG, "OpenTokApiKey");
            var secret = ConfigReader.GetAppSetting(CONFIG, "OpenTokSecret");
            _apiKey = Convert.ToInt32(apiKey);
            _openTok = new OpenTok(_apiKey, secret);

        }

        /// <summary>
        /// Creates a session creates an object that can be send as FCM payload
        /// </summary>
        /// <param name="recipient">Username of the recipient</param>
        /// <returns></returns>
        public OpenTokInfo CreateSession(  int ticketNumber)
        {
            var session = _openTok.CreateSession();
            var info = new OpenTokInfo
            {
                ApiKey = _apiKey,
                SessionId = session.Id,
                Token = session.GenerateToken(),
                TicketNumber = ticketNumber
            };

            return info;
        }

    }
}