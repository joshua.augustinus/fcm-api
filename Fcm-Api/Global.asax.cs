﻿using Fcm_Api.Helpers;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Tms.Api.Common;

namespace Fcm_Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            HttpConfiguration config = GlobalConfiguration.Configuration;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new IdToStringConverter());
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            //This next line means that all your date times that you return from the API are assumed to be UTC and
            //the serialiser will automatically add a Z at the end
            config.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Utc;



            FcmHelper.Init();
            var url = ConfigurationManager.AppSettings["AUTH_API_URL"];
            Decrypter.GetInstance().Setup(url);
        }
    }
}
