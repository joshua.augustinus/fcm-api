﻿using Fcm_Api.DataObjects;
using Fcm_Api.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Fcm_Api.Filters
{
    public class BearerAuth : AuthorizeAttribute
    {
        protected override bool IsAuthorized(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            try
            {
                var authString = actionContext.ControllerContext.Request.Headers.GetValues("Authorization");
                if (authString != null)
                {
                    var codedString = authString.First().Split(' ');
                    var authenticationType = codedString[0];
                    if (authenticationType.ToLower().Equals("bearer"))
                    {
                        var token = codedString[1];
                        var payload = Decrypter.GetInstance().DecodeToken(token);
                        BearerAuth.SetPayload(payload);
                        

                        return true;
                    }
                }

                return false;
            }
            catch(Exception e)
            {
                return false;
            }
         }

        public static void SetPayload(object payload)
        {
            HttpContext.Current.Items["payload"] = payload;
        }

        public static JWTokenPayload GetPayload()
        {
            var result = HttpContext.Current.Items["payload"];
            if (result == null)
            {
                throw new NotImplementedException();
                //it should not go here because if payload is null the auth would fail
            }

            return (JWTokenPayload)result;
        }
    }
}