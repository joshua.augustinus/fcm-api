# About
This API handles:
- Creating of video conferencing sessions using opentok library.


# Setup
1. Add secrets.config to the same folder as web.config. This contains the api key for opentok. This file is not checked into source control for security reasons. Ask the owner for the file.


# Adding common-nuget-packages

Some projects use the same common files. These common files have been packaged into a nuget package.

```
git clone https://gitlab.com/joshua.augustinus/common-nuget-packages.git
```

1. Open Visual Studio.
2. Go to Tools > Nuget package manager > Package manager settings > Package sources
3. Add a package source. Name should be "Local" and the source should be the folder where you downloaded the common-nuget-packages.
4. You should now be able to build the solution and it will automatically use the packages in your Local packge source.
